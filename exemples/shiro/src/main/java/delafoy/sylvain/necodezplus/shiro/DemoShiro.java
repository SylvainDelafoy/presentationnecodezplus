package delafoy.sylvain.necodezplus.shiro;

import java.security.Key;

import org.apache.shiro.codec.Base64;
import org.apache.shiro.codec.CodecSupport;
import org.apache.shiro.codec.Hex;
import org.apache.shiro.crypto.AesCipherService;
import org.apache.shiro.util.ByteSource;

public class DemoShiro {
	private static final AesCipherService SERVICE = new AesCipherService();

	public static void main(final String[] args) {
		final Key key = SERVICE.generateNewKey();
		System.out.println("Algo:" + key.getAlgorithm() + " Clef: "
				+ Base64.encodeToString(key.getEncoded()));
		final byte[] transmission = SERVICE.encrypt(
				CodecSupport.toBytes("Le PHP? J'ai rien contre."),
				key.getEncoded()).getBytes();
		System.out.println("Hex   :" + Hex.encodeToString(transmission));
		System.out.println("String:" + new String(transmission));
		final ByteSource message = SERVICE.decrypt(transmission,
				key.getEncoded());
		System.out.println(CodecSupport.toString(message.getBytes()));
	}

}
