package com.valtech.presentation.codezmoins.trywithressources;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.FileWriter;
import java.io.IOException;

import org.mockito.Mockito;

import com.google.common.base.Supplier;
import com.valtech.presentation.codezmoins.trywithressources.exceptions.DummyCloseException;
import com.valtech.presentation.codezmoins.trywithressources.exceptions.DummyWriteException;

public class FileWriterMockBuilder implements Supplier<FileWriter> {
	public FileWriterMockBuilder() {
		instance = mock(FileWriter.class);
	}

	private final FileWriter instance;

	@Override
	public FileWriter get() {
		return instance;
	}

	public void checkWriterClosed() {
		try {
			verify(instance, atLeast(1)).close();
		} catch (final IOException e) {
			// Not in mocks
		}
	}

	@SuppressWarnings("unchecked")
	public void enableWriteFaillure() {
		try {
			when(instance.append(any(String.class))).thenThrow(
					DummyWriteException.class);
		} catch (final IOException e) {
			// Not in mocks
		}
	}

	public void enableCloseFaillure() {
		try {
			Mockito.doThrow(DummyCloseException.class).when(instance).close();
		} catch (final IOException e) {
			// Not in mocks
		}
	}
}