package com.valtech.presentation.codezmoins.trywithressources.exceptions;

import java.io.IOException;

public class DummyCloseException extends IOException {
	private static final long serialVersionUID = 7213683170337114973L;

	public DummyCloseException() {
		super("Oh noes the close operation failled !");
	}
}