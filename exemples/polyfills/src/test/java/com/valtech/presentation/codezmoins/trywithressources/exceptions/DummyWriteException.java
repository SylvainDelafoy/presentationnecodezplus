package com.valtech.presentation.codezmoins.trywithressources.exceptions;

import java.io.IOException;

public class DummyWriteException extends IOException {
	private static final long serialVersionUID = -4668031238206017856L;

	public DummyWriteException() {
		super("Oh noes the write operation failled !");
	}
}