package com.valtech.presentation.codezmoins.profile;

import java.io.IOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import com.google.common.io.CharSource;
import com.google.common.io.Resources;
import com.valtech.presentation.codezmoins.profile.Matcher.Matcher2;

@RunWith(Parameterized.class)
public class MatcherTest {

	@Parameters(name = "{0}")
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { /*
											 * { Matcher1.class } ,
											 */{ Matcher2.class } });
	}

	@Parameter
	public Class<? extends Matcher> testedMethod;

	@BeforeClass
	public static void justWait() throws InterruptedException {
		Thread.sleep(30000L);
	}

	private static final Set<String> words = loadWords();

	private static Set<String> loadWords() {
		final Predicate<String> isAllLetters = Pattern.compile("[a-zA-Z]+")
				.asPredicate();
		try {
			final URL url = new URL("http://www.manpagez.com/man/1/java/");
			final CharSource wordsSource = Resources.asCharSource(url,
					Charset.defaultCharset());
			final Stream<String> words = wordsSource.readLines().stream()//
					.flatMap(s -> Arrays.stream(s.split("\\b")))//
					.filter(isAllLetters)//
					.map((s) -> s.toLowerCase());
			return words.collect(Collectors.toCollection(TreeSet::new));
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Test
	public void run() throws IOException, InterruptedException,
	InstantiationException, IllegalAccessException {
		final Matcher tested = testedMethod.newInstance();
		for (int i = 0; i < 10000; i++) {
			for (final String word : words) {
				if (tested.match(word)) {
					System.out.println(word);
				}
			}
		}
	}
}
