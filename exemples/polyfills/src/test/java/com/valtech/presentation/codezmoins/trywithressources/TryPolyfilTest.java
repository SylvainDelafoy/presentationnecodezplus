package com.valtech.presentation.codezmoins.trywithressources;

import static org.fest.assertions.Assertions.assertThat;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

import com.valtech.presentation.codezmoins.trywithressources.exceptions.DummyCloseException;
import com.valtech.presentation.codezmoins.trywithressources.exceptions.DummyWriteException;

@RunWith(Parameterized.class)
public class TryPolyfilTest {

	@Parameters(name = "{0}")
	public static Collection<Object[]> data() {
		return Arrays
				.asList(new Object[][] { { TryPolyfil.Java.class },
						{ TryPolyfil.IOUtilsWay.class },
						{ TryPolyfil.GuavaWay.class } });
	}

	@Parameter
	public Class<? extends TryPolyfil> testedMethod;

	private TryPolyfil undertest;
	private FileWriterMockBuilder supplier;

	@Before
	public void before() throws InstantiationException, IllegalAccessException {
		undertest = testedMethod.newInstance();

		supplier = new FileWriterMockBuilder();
		undertest.writerSupplier = supplier;
	}

	@Test
	public void testNominal() throws Exception {
		// Given
		// When
		undertest.write();
		// Then
		supplier.checkWriterClosed();
	}

	@Test
	public void testFailWrite() throws Exception {
		// Given
		supplier.enableWriteFaillure();
		// When
		try {
			undertest.write();
			// Then
			Assert.fail("should have failed");
		} catch (final Throwable e) {
			assertThat(e).isInstanceOf(DummyWriteException.class);
			supplier.checkWriterClosed();
		}
	}

	@Test
	public void testFailClose() throws Exception {
		// Given
		supplier.enableCloseFaillure();
		;
		// When
		try {
			undertest.write();
			// Then
			Assert.fail("should have failed");
		} catch (final Throwable e) {
			assertThat(e).isInstanceOf(DummyCloseException.class);
			supplier.checkWriterClosed();
		}
	}

	@Test
	public void testFailBoth() throws Exception {
		// Given
		supplier.enableWriteFaillure();
		supplier.enableCloseFaillure();
		// When
		try {
			undertest.write();
			// Then
			Assert.fail("should have failed");
		} catch (final Throwable e) {
			assertThat(e).isInstanceOf(DummyWriteException.class);
			supplier.checkWriterClosed();
		}
	}
}
