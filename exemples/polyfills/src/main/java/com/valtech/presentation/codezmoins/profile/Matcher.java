package com.valtech.presentation.codezmoins.profile;

import java.util.regex.Pattern;

public interface Matcher {

	boolean match(String string);

	public class Matcher1 implements Matcher {
		@Override
		public boolean match(final String string) {
			return string.matches("aa");
		}
	}

	public class Matcher2 implements Matcher {
		Pattern pattern = Pattern.compile("aa");

		@Override
		public boolean match(final String string) {
			return pattern.matcher(string).matches();
		}
	}
}
