package com.valtech.presentation.codezmoins.trywithressources;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.apache.commons.io.IOUtils;

import com.google.common.base.Supplier;
import com.google.common.io.Closer;

public abstract class TryPolyfil_solution {

	Supplier<FileWriter> writerSupplier;

	public abstract void write() throws IOException;

	public static class Java extends TryPolyfil_solution {
		@Override
		public void write() throws IOException {
			try (Writer writer = writerSupplier.get()) {
				writer.append("test");
			}
		}
	}

	public static class IOUtilsWay extends TryPolyfil_solution {
		@Override
		public void write() throws IOException {
			Writer writer = null;
			try {
				writer = writerSupplier.get();
				writer.append("test");
				writer.close();
			} finally {
				IOUtils.closeQuietly(writer);
			}
		}
	}

	public static class GuavaWay extends TryPolyfil_solution {
		@Override
		public void write() throws IOException {
			final Closer closer = Closer.create();
			try {
				final FileWriter in = closer.register(writerSupplier.get());
				in.append("test");
			} catch (final Throwable e) {
				// Cast en RuntimeException si ce n'est pas une IOException;
				throw closer.rethrow(e);
			} finally {
				closer.close();
			}
		}
	}
}