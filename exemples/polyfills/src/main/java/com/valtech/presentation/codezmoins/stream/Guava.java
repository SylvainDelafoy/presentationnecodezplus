package com.valtech.presentation.codezmoins.stream;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.google.common.collect.HashBiMap;
import com.google.common.collect.Lists;

public class Guava {
	public void flux() {
		final List<Integer> l = Arrays.asList(1, 2, 3);
		final List<String> transform = Lists.transform(l,
				input -> input.toString());
		for (final String string : transform) {
			System.out.println(string);
		}
	}

	public static void main(final String[] args) {
		new Guava().biMap();
	}

	public void biMap() {
		final HashMap<Object, Object> map = new HashMap<>();
		map.put("key1", "val1");
		map.put("key2", "val2");
		final HashBiMap<Object, Object> biMap = HashBiMap.create(map);
		System.out.println(biMap.inverse().get("val2").equals("key2"));
	}
}
