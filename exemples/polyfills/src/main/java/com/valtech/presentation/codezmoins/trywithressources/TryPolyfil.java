package com.valtech.presentation.codezmoins.trywithressources;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import org.apache.commons.io.IOUtils;

import com.google.common.base.Supplier;
import com.google.common.io.Closer;

public abstract class TryPolyfil {

	Supplier<FileWriter> writerSupplier;

	public abstract void write() throws IOException;

	public static class Java extends TryPolyfil {
		@Override
		public void write() throws IOException {
			try (Writer writer = writerSupplier.get()) {
				writer.append("test");
			}
		}
	}

	public static class IOUtilsWay extends TryPolyfil {
		@Override
		public void write() throws IOException {
			Writer w = null;
			try {
				w = writerSupplier.get();
				w.append("test");
				w.close();
			} finally {
				IOUtils.closeQuietly(w);
			}
		}
	}

	public static class GuavaWay extends TryPolyfil {
		@Override
		public void write() throws IOException {
			final Closer c = Closer.create();
			try {
				final Writer w = writerSupplier.get();
				c.register(w);
				w.append("test");
			} catch (final IOException e) {
				c.rethrow(e);
			} finally {
				c.close();
			}
		}
	}
}