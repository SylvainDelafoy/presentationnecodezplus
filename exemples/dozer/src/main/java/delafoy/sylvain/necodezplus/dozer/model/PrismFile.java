package delafoy.sylvain.necodezplus.dozer.model;

import java.util.Date;
import java.util.List;

/*J'aurais aussi accept� PRISMFile*/
public class PrismFile {

	public static class Experience {
		public Experience() {
		}

		private String entreprise;

		public String getEntreprise() {
			return entreprise;
		}

		public void setEntreprise(final String entreprise) {
			this.entreprise = entreprise;
		}

		private Date debut;

		public Date getDebut() {
			return debut;
		}

		public void setDebut(final Date debut) {
			this.debut = debut;
		}

		private Date fin;

		public Date getFin() {
			return fin;
		}

		public void setFin(final Date fin) {
			this.fin = fin;
		}
	}

	private String firstName;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	private String lastName;

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	private Date birthday;

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(final Date birthday) {
		this.birthday = birthday;
	}

	private List<Experience> experiences;

	public List<Experience> getExperiences() {
		return experiences;
	}

	public void setExperiences(final List<Experience> experiences) {
		this.experiences = experiences;
	}

	private Experience firstExperience;

	public Experience getFirstExperience() {
		return firstExperience;
	}

	public void setFirstExperience(final Experience firstExperience) {
		this.firstExperience = firstExperience;
	}
}
