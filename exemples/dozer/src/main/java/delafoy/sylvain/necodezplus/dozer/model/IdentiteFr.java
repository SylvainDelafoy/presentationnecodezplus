package delafoy.sylvain.necodezplus.dozer.model;

import java.util.Date;

public class IdentiteFr {
    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    private String nom;

    public String getNom() {
        return this.nom;
    }

    public void setNom(final String firstName) {
        this.nom = firstName;
    }

    private String prenom;

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(final String lastName) {
        this.prenom = lastName;
    }

    private Date anniversaire;

    public Date getAnniversaire() {
        return this.anniversaire;
    }

    public void setAnniversaire(final Date birthday) {
        this.anniversaire = birthday;
    }

    private int numCID;

    public int getNumCID() {
        return this.numCID;
    }

    public void setNumCID(final int cardNumber) {
        this.numCID = cardNumber;
    }

}
