package delafoy.sylvain.necodezplus.dozer.model;

import java.util.List;

public class Profil {

	private String firstName;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(final String firstName) {
		this.firstName = firstName;
	}

	private String lastName;

	public String getLastName() {
		return lastName;
	}

	public void setLastName(final String lastName) {
		this.lastName = lastName;
	}

	private List<String> experiences;

	public List<String> getExperiences() {
		return experiences;
	}

	public void setExperiences(final List<String> experiences) {
		this.experiences = experiences;
	}

}
