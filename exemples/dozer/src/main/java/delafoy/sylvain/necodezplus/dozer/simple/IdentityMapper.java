package delafoy.sylvain.necodezplus.dozer.simple;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import delafoy.sylvain.necodezplus.dozer.model.Identity;
import delafoy.sylvain.necodezplus.dozer.model.Profil;

public class IdentityMapper {
    private static final Mapper MAPPER = new DozerBeanMapper();

    public Profil doMap(final Identity identity) {
        return IdentityMapper.MAPPER.map(identity, Profil.class);
    }
}
