package delafoy.sylvain.necodezplus.dozer.xml;

import java.util.Arrays;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import delafoy.sylvain.necodezplus.dozer.model.IdentiteFr;
import delafoy.sylvain.necodezplus.dozer.model.Identity;
import delafoy.sylvain.necodezplus.dozer.model.PrismFile;
import delafoy.sylvain.necodezplus.dozer.model.Profil;

public class IdentityToFrMapper {
	private static final Mapper MAPPER = IdentityToFrMapper.buildMapper();

	private static Mapper buildMapper() {
		final DozerBeanMapper dozerBeanMapper = new DozerBeanMapper();
		dozerBeanMapper.setMappingFiles(Arrays
				.asList("delafoy/sylvain/necodezplus/dozer/mappings.xml"));
		return dozerBeanMapper;
	}

	public IdentiteFr doMap(final Identity identity) {
		return IdentityToFrMapper.MAPPER.map(identity, IdentiteFr.class);
	}

	public PrismFile doSpy(final Identity identite, final Profil profil) {
		final PrismFile destination = new PrismFile();
		MAPPER.map(profil, destination);
		MAPPER.map(identite, destination);
		return destination;
	}
}
