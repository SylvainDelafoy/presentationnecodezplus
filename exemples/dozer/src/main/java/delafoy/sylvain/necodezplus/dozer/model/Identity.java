package delafoy.sylvain.necodezplus.dozer.model;

import java.util.Date;

public class Identity {
    private Long id;

    public Long getId() {
        return this.id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    private String firstName;

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    private String lastName;

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    private Date birthday;

    public Date getBirthday() {
        return this.birthday;
    }

    public void setBirthday(final Date birthday) {
        this.birthday = birthday;
    }

    private int cardNumber;

    public int getCardNumber() {
        return this.cardNumber;
    }

    public void setCardNumber(final int cardNumber) {
        this.cardNumber = cardNumber;
    }

}
