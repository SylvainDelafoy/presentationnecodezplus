package delafoy.sylvain.necodezplus.dozer.xml;

import java.util.Arrays;
import java.util.Date;

import org.fest.assertions.Assertions;
import org.junit.Test;

import delafoy.sylvain.necodezplus.dozer.model.IdentiteFr;
import delafoy.sylvain.necodezplus.dozer.model.Identity;
import delafoy.sylvain.necodezplus.dozer.model.PrismFile;
import delafoy.sylvain.necodezplus.dozer.model.Profil;

public class IdentityToFrMapperTest {
	IdentityToFrMapper undertest = new IdentityToFrMapper();

	@Test
	public void testDoMap() throws Exception {
		//Given
		final Identity identity = buildIdentity();
		
		//When
		final IdentiteFr profil = undertest.doMap(identity);
		//Then
		Assertions.assertThat(profil.getNom())//
		.isEqualTo(identity.getFirstName());
		Assertions.assertThat(profil.getPrenom())//
				.isEqualTo(identity.getLastName());
		Assertions.assertThat(profil.getNumCID())//
				.isEqualTo(identity.getCardNumber());
		Assertions.assertThat(profil.getAnniversaire())//
		.isEqualTo(identity.getBirthday());
		Assertions.assertThat(profil.getId()).isNull();
	}

	private Identity buildIdentity() {
		final Identity identity = new Identity();
		identity.setFirstName("first name");
		identity.setLastName("last name");
		identity.setBirthday(new Date());
		identity.setCardNumber(1);
		identity.setId(5L);
		return identity;
	}

	@Test
	public void testDoSpy() throws Exception {
		//Given
		final Identity identity = buildIdentity();
		final Profil profil = new Profil();
		profil.setExperiences(Arrays.asList("exp1", "exp2", "exp3"));
		//When
		final PrismFile spy = undertest.doSpy(identity, profil);
		//Then
		Assertions.assertThat(spy.getFirstName()).isEqualTo(
				identity.getFirstName());
		Assertions.assertThat(spy.getLastName()).isEqualTo(
				identity.getLastName());
		Assertions.assertThat(spy.getBirthday()).isEqualTo(
				identity.getBirthday());
		Assertions.assertThat(spy.getFirstExperience().getEntreprise())
				.isEqualTo("exp1");
	}
}
