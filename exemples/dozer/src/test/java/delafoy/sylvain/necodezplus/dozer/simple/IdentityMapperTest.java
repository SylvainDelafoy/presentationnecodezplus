package delafoy.sylvain.necodezplus.dozer.simple;

import org.fest.assertions.Assertions;
import org.junit.Test;

import delafoy.sylvain.necodezplus.dozer.model.Identity;
import delafoy.sylvain.necodezplus.dozer.model.Profil;
import delafoy.sylvain.necodezplus.dozer.simple.IdentityMapper;

public class IdentityMapperTest {
    IdentityMapper undertest = new IdentityMapper();

    @Test
    public void testDoMap() throws Exception {
    	//Given
        final Identity identity = new Identity();
        identity.setFirstName("first name");
        identity.setLastName("last name");
        //When
        final Profil profil = this.undertest.doMap(identity);
        //Then
        Assertions.assertThat(profil.getFirstName()).isEqualTo(
                identity.getFirstName());
        Assertions.assertThat(profil.getLastName()).isEqualTo(
                identity.getLastName());
        Assertions.assertThat(profil.getExperiences()).isNull();
    }

}
