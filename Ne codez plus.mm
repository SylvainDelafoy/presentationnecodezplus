<map version="freeplane 1.3.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Ne codez plus" ID="ID_1723255651" CREATED="1412166820262" MODIFIED="1412166820262"><hook NAME="MapStyle">

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node">
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right">
<stylenode LOCALIZED_TEXT="default" MAX_WIDTH="600" COLOR="#000000" STYLE="as_parent">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.note"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="3"/>
<node TEXT="Recherche" POSITION="left" ID="ID_489979313" CREATED="1412166820262" MODIFIED="1412166820262">
<edge COLOR="#ffff00"/>
<node TEXT="A creuser" ID="ID_39865898" CREATED="1412166820262" MODIFIED="1412166820262">
<node TEXT="http://projects.spring.io/spring-data-jpa/" ID="ID_336267738" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://projects.spring.io/spring-data-jpa/"/>
<node TEXT="http://projects.spring.io/spring-data/" ID="ID_551512382" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://projects.spring.io/spring-data/"/>
<node TEXT="http://projects.spring.io/spring-security/" ID="ID_1701659727" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://projects.spring.io/spring-security/"/>
<node TEXT="http://projects.spring.io/spring-social/" ID="ID_139263912" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://projects.spring.io/spring-social/"/>
<node TEXT="https://spring.io/projects" ID="ID_260264859" CREATED="1412166820262" MODIFIED="1412166820262" LINK="https://spring.io/projects"/>
</node>
<node TEXT="Cache" ID="ID_1206609138" CREATED="1412166820262" MODIFIED="1412166820262">
<node TEXT="http://commons.apache.org/proper/commons-jcs/" ID="ID_689276396" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://commons.apache.org/proper/commons-jcs/"/>
<node TEXT="http://commons.apache.org/proper/commons-pool/examples.html" ID="ID_723438491" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://commons.apache.org/proper/commons-pool/examples.html"/>
<node TEXT="https://code.google.com/p/guava-libraries/wiki/CachesExplained#Eviction" ID="ID_961457330" CREATED="1412166820262" MODIFIED="1412166820262" LINK="https://code.google.com/p/guava-libraries/wiki/CachesExplained#Eviction"/>
</node>
<node TEXT="En g&#xe9;n&#xe9;ral" ID="ID_1516991167" CREATED="1412166820262" MODIFIED="1412166820262">
<node TEXT="http://commons.apache.org/" ID="ID_991103606" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://commons.apache.org/"/>
<node TEXT="https://code.google.com/p/guava-libraries/wiki/GuavaExplained?tm=6" ID="ID_1297580910" CREATED="1412166820262" MODIFIED="1412166820262" LINK="https://code.google.com/p/guava-libraries/wiki/GuavaExplained?tm=6"/>
</node>
<node TEXT="Fichiers" ID="ID_239665833" CREATED="1412166820262" MODIFIED="1412166820262">
<node TEXT="http://commons.apache.org/proper/commons-io/" ID="ID_904430460" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://commons.apache.org/proper/commons-io/"/>
<node TEXT="http://commons.apache.org/proper/commons-vfs/api.html" ID="ID_1628783932" CREATED="1412166820262" MODIFIED="1412166820262"/>
<node TEXT="https://github.com/google/jimfs" ID="ID_52729885" CREATED="1412166820262" MODIFIED="1412166820262" LINK="https://github.com/google/jimfs"/>
</node>
<node TEXT="CSS" ID="ID_763189610" CREATED="1412166820262" MODIFIED="1412166820262">
<node TEXT="http://twbs.github.io/bootstrap/" ID="ID_1385321731" CREATED="1412166820262" MODIFIED="1412166820262"/>
</node>
<node TEXT="https://parleys.com/play/514892290364bc17fc56c54d" ID="ID_938015174" CREATED="1412166820262" MODIFIED="1412166820262" LINK="https://parleys.com/play/514892290364bc17fc56c54d"/>
<node TEXT="https://www.parleys.com/play/517ac20ae4b0c6dcd954645b" ID="ID_673453333" CREATED="1412166820262" MODIFIED="1412166820262" LINK="https://www.parleys.com/play/517ac20ae4b0c6dcd954645b"/>
<node TEXT="I18n" ID="ID_845035402" CREATED="1412166820262" MODIFIED="1412166820262">
<node TEXT="http://site.icu-project.org/" ID="ID_1550869034" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://site.icu-project.org/"/>
</node>
<node TEXT="JS" ID="ID_1512278769" CREATED="1412166820262" MODIFIED="1412166820262">
<node TEXT="templating" ID="ID_1783951976" CREATED="1412166820262" MODIFIED="1412166820262">
<node TEXT="https://github.com/janl/mustache.js" ID="ID_1190157642" CREATED="1412166820262" MODIFIED="1412166820262"/>
</node>
<node TEXT="yeoman" ID="ID_1546158350" CREATED="1412166820262" MODIFIED="1412166820262">
<node TEXT="Il faut avoir install&#xe9; git avant d&apos;installer npm" ID="ID_311123654" CREATED="1412166820262" MODIFIED="1412166820262"/>
<node TEXT="ensuite installer npm" ID="ID_1683413974" CREATED="1412166820262" MODIFIED="1412166820262"/>
<node TEXT="$ npm install -g yo" ID="ID_1548690397" CREATED="1412166820262" MODIFIED="1412166820262"/>
<node TEXT="npm install -g generator-webapp" ID="ID_592890008" CREATED="1412166820262" MODIFIED="1412166820262"/>
</node>
<node TEXT="http://facebook.github.io/jest/" ID="ID_1329861003" CREATED="1412166820262" MODIFIED="1412166820262"/>
<node TEXT="http://taskjs.org/" ID="ID_1545744700" CREATED="1412166820262" MODIFIED="1412166820262"/>
<node TEXT="http://modernizr.com/docs/" ID="ID_190624897" CREATED="1412166820262" MODIFIED="1412166820262"/>
<node TEXT="mathjs.org" ID="ID_840696015" CREATED="1412166820262" MODIFIED="1412166820262"/>
<node TEXT="http://montagestudio.com/montagejs/" ID="ID_1322493794" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://montagestudio.com/montagejs/"/>
<node TEXT="http://en.wikipedia.org/wiki/Dojo_Toolkit" ID="ID_1489708454" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://en.wikipedia.org/wiki/Dojo_Toolkit"/>
<node TEXT="http://en.wikipedia.org/wiki/Ember.js" ID="ID_493875961" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://en.wikipedia.org/wiki/Ember.js"/>
<node TEXT="http://en.wikipedia.org/wiki/Prototype_JavaScript_Framework" ID="ID_1041321559" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://en.wikipedia.org/wiki/Prototype_JavaScript_Framework"/>
<node TEXT="http://en.wikipedia.org/wiki/Meteor_%28web_framework%29" ID="ID_365551161" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://en.wikipedia.org/wiki/Meteor_%28web_framework%29"/>
<node TEXT="http://wiki.openstreetmap.org/wiki/OpenLayers_Simple_Example" ID="ID_1408649891" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://wiki.openstreetmap.org/wiki/OpenLayers_Simple_Example"/>
<node TEXT="http://leafletjs.com/examples/quick-start.html" ID="ID_223569513" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://leafletjs.com/examples/quick-start.html"/>
<node TEXT="https://github.com/mojotech/pioneer/blob/master/docs/getting_started.md" ID="ID_1838340576" CREATED="1412166820262" MODIFIED="1412166820262" LINK="https://github.com/mojotech/pioneer/blob/master/docs/getting_started.md"/>
<node TEXT="http://adodson.com/hello.js/" ID="ID_1827082236" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://adodson.com/hello.js/"/>
<node TEXT="https://www.google.fr/search?q=javascript+internationalization&amp;ie=utf-8&amp;oe=utf-8&amp;aq=t&amp;rls=org.mozilla:fr-FR:official&amp;client=firefox-a&amp;channel=sb&amp;gfe_rd=cr&amp;ei=PYAYVOquNuOA8QfS74HYAQ" ID="ID_36986757" CREATED="1412166820262" MODIFIED="1412166820262"/>
<node TEXT="https://github.com/ericlemerdy/one-kata-per-day/commit/a016acc9b8eb17984368238cabfb552b8b4610c1" ID="ID_322351139" CREATED="1412166820262" MODIFIED="1412166820262"/>
<node TEXT="http://facebook.github.io/jest/" ID="ID_1183114947" CREATED="1412166820262" MODIFIED="1412166820262"/>
<node TEXT="http://pioneerjs.com/" ID="ID_282286205" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://pioneerjs.com/"/>
<node TEXT="https://github.com/browserstate/history.js/" ID="ID_1433367032" CREATED="1412166820262" MODIFIED="1412166820262" LINK="https://github.com/browserstate/history.js/"/>
<node TEXT="http://jlongster.com/Transducers.js--A-JavaScript-Library-for-Transformation-of-Data" ID="ID_835157258" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://jlongster.com/Transducers.js--A-JavaScript-Library-for-Transformation-of-Data"/>
</node>
<node TEXT="Langage" ID="ID_1910939657" CREATED="1412166820262" MODIFIED="1412166820262">
<icon BUILTIN="button_cancel"/>
<node TEXT="http://commons.apache.org/proper/commons-lang/" ID="ID_330163078" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://commons.apache.org/proper/commons-lang/"/>
<node TEXT="http://commons.apache.org/proper/commons-beanutils/javadocs/v1.9.2/apidocs/index.html" ID="ID_698406900" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://commons.apache.org/proper/commons-beanutils/javadocs/v1.9.2/apidocs/index.html"/>
<node TEXT="http://morph.sourceforge.net/" ID="ID_1595208164" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://morph.sourceforge.net/"/>
<node TEXT="http://beanlib.sourceforge.net/5.0.5/api/" ID="ID_1464748978" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://beanlib.sourceforge.net/5.0.5/api/"/>
<node TEXT="http://commons.apache.org/proper/commons-el/" ID="ID_1936629172" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://commons.apache.org/proper/commons-el/"/>
</node>
<node TEXT="REST" ID="ID_952948314" CREATED="1412166820262" MODIFIED="1412166820262">
<node TEXT="https://spring.io/blog/2014/08/21/building-a-restful-quotation-service-with-spring" ID="ID_1962097267" CREATED="1412166820262" MODIFIED="1412166820262" LINK="https://spring.io/blog/2014/08/21/building-a-restful-quotation-service-with-spring">
<icon BUILTIN="button_cancel"/>
</node>
</node>
<node TEXT="test" ID="ID_1027592265" CREATED="1412166820262" MODIFIED="1412166820262">
<node TEXT="http://blog.octo.com/mutation-testing-un-pas-de-plus-vers-la-perfection/" ID="ID_1972018905" CREATED="1412166820262" MODIFIED="1412166820262" LINK="http://blog.octo.com/mutation-testing-un-pas-de-plus-vers-la-perfection/"/>
<node TEXT="https://joel-costigliola.github.io/assertj/" ID="ID_816603068" CREATED="1412166820272" MODIFIED="1412166820272" LINK="https://joel-costigliola.github.io/assertj/"/>
</node>
<node TEXT="Tout" ID="ID_1710634722" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="https://jhipster.github.io/installation.html" ID="ID_123119345" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="javamind-fr.blogspot.fr/2014/08/spring-boot-ou-comment-demarrer-une.html" ID="ID_1573801156" CREATED="1412166820272" MODIFIED="1412166820272"/>
</node>
</node>
<node TEXT="Intro" POSITION="right" ID="ID_241139566" CREATED="1412166820272" MODIFIED="1412252020032">
<edge COLOR="#00ff00"/>
<node TEXT="Tel que je le vois,Notre travail consiste &#xe0; construire des solutions &#xe0; de gros probl&#xe8;mes &#xe0; partir de solutions &#xe0; de petits probl&#xe8;mes." ID="ID_1003257013" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="On &#xe9;crit rarement un nouvel algo" ID="ID_283042824" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="On rencontre rarement des probl&#xe8;mes qui se r&#xe9;solvent via un parcours de graph" ID="ID_1709158631" CREATED="1412166820272" MODIFIED="1412166820272"/>
</node>
<node TEXT="La plupart du temps" ID="ID_1056508117" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="On &#xe9;crit des Interfaces graphiques" ID="ID_253447612" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="On fait du crud" ID="ID_1680520625" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="On met &#xe0; jour des champs &#xe0; partir de formules et de valeurs d&apos;autre champs" ID="ID_750655928" CREATED="1412166820272" MODIFIED="1412251999382"/>
</node>
<node TEXT="Au final, on &#xe9;crit" ID="ID_583366367" CREATED="1412166820272" MODIFIED="1412251874831">
<node TEXT="des DAO qui ressemblent exactement au DAO qu&apos;on &#xe0; &#xe9;crit hier" ID="ID_120555627" CREATED="1412166820272" MODIFIED="1412251879371"/>
<node TEXT="On &#xe9;crit du code qui transforme un objet en un autre objet" ID="ID_1615893226" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="Une entity dans un DTO" ID="ID_1722238723" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="Et inversement" ID="ID_1691087188" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="Quand on a de la chance, on r&#xe9;cup&#xe8;re des objets d&apos;autres application de l&apos;entreprise" ID="ID_623410835" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="On les fusionne" ID="ID_1096309884" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="On les aminci" ID="ID_1136146752" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="On convertie" ID="ID_522482341" CREATED="1412166820272" MODIFIED="1412166820272"/>
</node>
</node>
</node>
<node TEXT="Je sais pas pour vous, mais moi j&apos;en ai marre" ID="ID_1974901248" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="D&apos;&#xe9;crire des DAO" ID="ID_524961035" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="D&apos;&#xe9;crire toujours le m&#xea;me code de mappage d&apos;une entit&#xe9; vers un DTO" ID="ID_26968909" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="De patienter parceque qu&apos;un test prend des plombes" ID="ID_1025674883" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="&#xe7;a arrive aussi" ID="ID_1602947931" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="Des fois j&apos;ai un algo qui n&apos;est pas bon" ID="ID_1241536182" CREATED="1412166820272" MODIFIED="1412166820272"/>
</node>
</node>
<node TEXT="et puis faut &#xea;tre honn&#xea;te, tout code &#xe9;crit, c&apos;est du code &#xe0; maintenir" ID="ID_1507445434" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="&quot;Measuring software productivity by lines of code is like measuring progress on an airplane by how much it weighs.&quot;- Bill Gates" ID="ID_1973412852" CREATED="1412166820272" MODIFIED="1412166820272"/>
</node>
<node TEXT="Des fois on rencontre un nouveau probl&#xe8;me" ID="ID_1746516968" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="Un truc un peu compliqu&#xe9;" ID="ID_1793112268" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="On se dit qu&apos;on va utiliser la nouvelle fonctionnalit&#xe9; Java/JS dont on a entendu parler aupr&#xe8;s des coll&#xe8;gues" ID="ID_33085330" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="Mais la prod ne suit pas" ID="ID_73939872" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="Ou vos utilsateurs utilisent un vieu navigateur" ID="ID_1413391601" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="et impl&#xe9;menter avec la version n-1 ce que la version n fait nativement." ID="ID_732429441" CREATED="1412166820272" MODIFIED="1412166820272"/>
</node>
</node>
<node TEXT="Avant toutes choses" ID="ID_1462789330" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="un coll&#xe8;gue &#xe0; peut &#xea;tre d&#xe9;j&#xe0; rencontr&#xe9; le probl&#xe8;me" ID="ID_1431286127" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="connaitre l&apos;architecture du projet" ID="ID_1009273602" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="librairie d&#xe9;j&#xe0; import&#xe9;e" ID="ID_1897329361" CREATED="1412166820272" MODIFIED="1412251918663">
<node TEXT="peut r&#xe9;gler le probl&#xe8;me" ID="ID_1861339751" CREATED="1412251919726" MODIFIED="1412251932253"/>
<node TEXT="Un coll&#xe8;gue utilise guava pour ses collections" ID="ID_1738800773" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="Et vous avez besoin d&apos;un eventbus" ID="ID_68248878" CREATED="1412166820272" MODIFIED="1412166820272"/>
</node>
<node TEXT="La solution existe d&#xe9;j&#xe0; ailleurs dans le code" ID="ID_1745592229" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="C&apos;est d&#xe9;j&#xe0; fait, allez boire un caf&#xe9;" ID="ID_1185227077" CREATED="1412166820272" MODIFIED="1412166820272"/>
</node>
</node>
</node>
<node TEXT="Spring" POSITION="right" ID="ID_767055835" CREATED="1412166820292" MODIFIED="1412252123912">
<edge COLOR="#00007c"/>
<node TEXT="http://spring.io/" ID="ID_4539406" CREATED="1412252156240" MODIFIED="1412252156240" LINK="http://spring.io/"/>
<node TEXT="Ce qu&apos;on retrouve le plus souvent maintenant, C&apos;est des choses connect&#xe9;es &#xe0; une BDD et qui exposent du rest" ID="ID_1636638831" CREATED="1412166820292" MODIFIED="1412166820292"/>
<node TEXT="Si c&apos;est du JEE, c&apos;est surement avec spring" ID="ID_1376345601" CREATED="1412166820292" MODIFIED="1412166820292"/>
<node TEXT="Est-ce bien utile de coder un DAO?" ID="ID_1717085636" CREATED="1412166820292" MODIFIED="1412252913493"/>
<node TEXT="Spring Data JPA - Spring Data Rest" ID="ID_148237836" CREATED="1412166820292" MODIFIED="1412210918287">
<node TEXT="Arr&#xe9;tez d&apos;&#xe9;crire des DAO ET du code pour Spring MVC" ID="ID_1133346946" CREATED="1412166820292" MODIFIED="1412166820292"/>
<node TEXT="Les DAO par l&apos;&#xe9;criture des interfaces" ID="ID_377863206" CREATED="1412251834431" MODIFIED="1412251843017"/>
<node TEXT="L&apos;exposition REST automatis&#xe9;e" ID="ID_1424856884" CREATED="1412251844447" MODIFIED="1412251856531"/>
<node TEXT="Projet exemple" ID="ID_203281973" CREATED="1412166820292" MODIFIED="1412252928882">
<icon BUILTIN="forward"/>
</node>
</node>
<node TEXT="En plus, on peut ne prendre que JPA et Rest, les s&#xe9;parer et mettre ses services entre les deux" ID="ID_698051328" CREATED="1412166820292" MODIFIED="1412166820292">
<node TEXT="Je ne vous demande pas pourquoi" ID="ID_1638839717" CREATED="1412166820292" MODIFIED="1412166820292"/>
</node>
<node TEXT="Sinon, y&apos;a d&apos;autres frameworks" ID="ID_1709125377" CREATED="1412166820292" MODIFIED="1412166820292">
<node TEXT="pour le wireing" ID="ID_846970944" CREATED="1412166820292" MODIFIED="1412166820292">
<node TEXT="https://github.com/google/guice" ID="ID_659287771" CREATED="1412166820292" MODIFIED="1412166820292" LINK="https://github.com/google/guice"/>
<node TEXT="http://square.github.io/dagger/" ID="ID_1812584021" CREATED="1412166820292" MODIFIED="1412166820292" LINK="http://square.github.io/dagger/"/>
</node>
<node TEXT="pour la s&#xe9;rialisation" ID="ID_767351620" CREATED="1412166820292" MODIFIED="1412166820292">
<node TEXT="https://code.google.com/p/google-gson/" ID="ID_5958615" CREATED="1412166820292" MODIFIED="1412166820292"/>
<node TEXT="jackson (mais il est utilis&#xe9; par spring)" ID="ID_100400649" CREATED="1412166820292" MODIFIED="1412166820292"/>
</node>
</node>
</node>
<node TEXT="SASS" POSITION="right" ID="ID_699092947" CREATED="1412166826510" MODIFIED="1412167045997">
<edge COLOR="#00ff00"/>
<node TEXT="Mare d&apos;&#xe9;crire du CSS" ID="ID_157856956" CREATED="1412167046446" MODIFIED="1412167360965"/>
<node TEXT="Oui, on peut s&apos;en servir si on a acc&#xe8;s &#xe0; maven." ID="ID_1317760678" CREATED="1412167361629" MODIFIED="1412167392186"/>
<node TEXT="Mais alors qu&apos;est-ce qu&apos;on y gagne" ID="ID_1109686929" CREATED="1412167392737" MODIFIED="1412167504289"/>
<node TEXT="Int&#xe9;r&#xea;ts:" ID="ID_45534508" CREATED="1412167504757" MODIFIED="1412167509769">
<node TEXT="Compatibilit&#xe9; navigateurs simplifi&#xe9;e" ID="ID_220778171" CREATED="1412167509775" MODIFIED="1412167519509"/>
<node TEXT="Factorisation du code" ID="ID_477503268" CREATED="1412167519918" MODIFIED="1412167524197"/>
<node TEXT="Et/ou classes CSS plus simples &#xe0; g&#xe9;rer" ID="ID_352804773" CREATED="1412167527765" MODIFIED="1412167541056"/>
</node>
<node TEXT="Projet exemple" ID="ID_1612233906" CREATED="1412169575253" MODIFIED="1412252945749">
<icon BUILTIN="forward"/>
</node>
</node>
<node TEXT="Dozer" POSITION="right" ID="ID_347553760" CREATED="1412166820292" MODIFIED="1412166820292">
<edge COLOR="#007c00"/>
<node TEXT="http://dozer.sourceforge.net/" ID="ID_1720587205" CREATED="1412166820292" MODIFIED="1412166820292" LINK="http://dozer.sourceforge.net/"/>
<node TEXT="Ne mappez plus." ID="ID_1400112387" CREATED="1412166820292" MODIFIED="1412166820292"/>
<node TEXT="Des objets pour la base, d&apos;autres pour Jackson, d&apos;autres qui impl&#xe9;mentent les interfaces de cette librairie d&apos;algos" ID="ID_1945783390" CREATED="1412166820292" MODIFIED="1412166820292"/>
<node TEXT="On peut en faire avec spring" ID="ID_1343975875" CREATED="1412166820292" MODIFIED="1412166820292">
<node TEXT="&lt;bean id=&quot;mapper&quot; class=&quot;org.dozer.DozerBeanMapper&quot;&gt;&#xa;  &lt;property name=&quot;mappingFiles&quot;&gt;&#xa;    &lt;list&gt;&#xa;      &lt;value&gt;dozer-global-configuration.xml&lt;/value&gt;   &#xa;      &lt;value&gt;dozer-bean-mappings.xml&lt;/value&gt;&#xa;      &lt;value&gt;more-dozer-bean-mappings.xml&lt;/value&gt;&#xa;    &lt;/list&gt;&#xa;  &lt;/property&gt;&#xa;&lt;/bean&gt;" ID="ID_358196552" CREATED="1412166820292" MODIFIED="1412166820292"/>
</node>
<node TEXT="&#xe7;a g&#xe8;re les sous types" ID="ID_400509765" CREATED="1412166820292" MODIFIED="1412166820292">
<node TEXT="avec des hints, on peut dire les sous-types A sont mapp&#xe9;s vers A&apos; et sous types B vers B&apos;" ID="ID_1055377438" CREATED="1412166820292" MODIFIED="1412166820292"/>
</node>
<node TEXT="On peut mapper des listes et des maps sur des objets" ID="ID_1841609463" CREATED="1412166820292" MODIFIED="1412166820292"/>
<node TEXT="Conversions String &lt;=&gt; date" ID="ID_944894331" CREATED="1412166820292" MODIFIED="1412166820292"/>
<node TEXT="On peut applatir une grape d&apos;objets" ID="ID_811389914" CREATED="1412166820292" MODIFIED="1412166820292"/>
</node>
<node TEXT="s&#xe9;curit&#xe9;" POSITION="right" ID="ID_1741726995" CREATED="1412166820292" MODIFIED="1412166820292">
<edge COLOR="#7c007c"/>
<node TEXT="Shiro" ID="ID_1042166852" CREATED="1412166820292" MODIFIED="1412166820292">
<node ID="ID_278196296" CREATED="1412166820292" MODIFIED="1412166820292" LINK="http://shiro.apache.org/"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      http://shiro.apache.org/
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="Il existe une version web" ID="ID_1833781737" CREATED="1412166820292" MODIFIED="1412166820292">
<node TEXT="Mais j&apos;avoue ne pas avoir adh&#xe9;rer &#xe0; la m&#xe9;thode" ID="ID_1407033676" CREATED="1412166820292" MODIFIED="1412166820292"/>
</node>
<node TEXT="Pas le temps de tout mettre en place" ID="ID_245093272" CREATED="1412166820292" MODIFIED="1412166820292"/>
<node TEXT="Si vous avez besoin de faire de la cryptographie sans prendre de risques" ID="ID_597061166" CREATED="1412166820292" MODIFIED="1412166820292">
<node TEXT="Les probl&#xe8;mes sont dans les impl&#xe9;mentations ou leur utilisation" ID="ID_283537784" CREATED="1412166820292" MODIFIED="1412166820292"/>
<node TEXT="Du coup, essayons de prendre une impl&#xe9;mentation commune &#xe0; tous" ID="ID_1077964103" CREATED="1412166820292" MODIFIED="1412166820292">
<node TEXT="Le JDK" ID="ID_1062917559" CREATED="1412166820292" MODIFIED="1412166820292"/>
</node>
<node TEXT="Et Pour l&apos;utilisation, partons du principe qu&apos;on veut des choses fiables" ID="ID_1495600633" CREATED="1412166820292" MODIFIED="1412166820292">
<node TEXT="Par exemple, pour AES, on &#xe0; un syst&#xe8;me d&apos;&quot;IV&quot; par d&#xe9;faut (similaire au SALT des algos de hash)" ID="ID_32411599" CREATED="1412166820292" MODIFIED="1412166820292"/>
</node>
<node TEXT="Et surtout plus simple" ID="ID_591862010" CREATED="1412166820292" MODIFIED="1412166820292"/>
<node TEXT="private static final AesCipherService SERVICE = new AesCipherService();" ID="ID_520437337" CREATED="1412166820292" MODIFIED="1412166820292"/>
<node TEXT="Key key = SERVICE.generateNewKey();" ID="ID_1981277710" CREATED="1412166820292" MODIFIED="1412166820292"/>
<node TEXT="ByteSource transmission = SERVICE.encrypt(CodecSupport.toBytes(&quot;Le PHP? J&apos;ai rien contre.&quot;),key.getEncoded()).getBytes();" ID="ID_111849521" CREATED="1412166820292" MODIFIED="1412166820292"/>
<node TEXT="final ByteSource message = SERVICE.decrypt(transmission.getBytes(), key.getEncoded()).getBytes();" ID="ID_632045827" CREATED="1412166820292" MODIFIED="1412166820292"/>
</node>
</node>
</node>
<node TEXT="Java" POSITION="right" ID="ID_1549854552" CREATED="1412166820272" MODIFIED="1412252113667">
<edge COLOR="#00ffff"/>
<node TEXT="Java est fourni avec des outils et des fonctionnalit&#xe9;s int&#xe9;ressantes de nos jours." ID="ID_544759563" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="Mais on est parfois oblig&#xe9; de rester compatible java6 chez nos clients" ID="ID_827256602" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="Il parrait qu&apos;il y en a qui ont leur prod en java7" ID="ID_591152703" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="Moi, je n&apos;ai vu que du 6" ID="ID_692041184" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="Petit tour" ID="ID_1547376492" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="Qui est en java 8" ID="ID_567623651" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="Qui est en java 7" ID="ID_1516550550" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="Qui est en java 6" ID="ID_244915312" CREATED="1412166820272" MODIFIED="1412166820272"/>
</node>
</node>
<node TEXT="&quot;polyfils&quot;/backports (10m)" ID="ID_1824043001" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="Des librairies qui vont apporter aux versions les plus anciennes de java des choses qui ont &#xe9;t&#xe9; apport&#xe9;es depuis." ID="ID_1661841244" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="Try with ressources (7m)" ID="ID_1911361848" CREATED="1412166820272" MODIFIED="1412166820272">
<icon BUILTIN="button_ok"/>
<node TEXT="D&#xe9;mo" ID="ID_215698751" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="try (Writer writer = writerSupplier.get()) {&#xa;    writer.append(&quot;test&quot;);&#xa;}" ID="ID_1736068569" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="Le try ouvre le flux, le bloc execute l&apos;action d&apos;&#xe9;criture et la fin de try ferme le flux" ID="ID_859887596" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="L&apos;id&#xe9;e est que la premi&#xe8;re exception lanc&#xe9;e est propag&#xe9;e" ID="ID_1157034407" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="http://docs.oracle.com/javase/7/docs/technotes/guides/language/try-with-resources.html" ID="ID_433443091" CREATED="1412166820272" MODIFIED="1412166820272" LINK="http://docs.oracle.com/javase/7/docs/technotes/guides/language/try-with-resources.html">
<node TEXT="Suppressed Exceptions&#xa;&#xa;An exception can be thrown from the block of code associated with the try-with-resources statement. In the example writeToFileZipFileContents, an exception can be thrown from the try block, and up to two exceptions can be thrown from the try-with-resources statement when it tries to close the ZipFile and BufferedWriter objects. If an exception is thrown from the try block and one or more exceptions are thrown from the try-with-resources statement, then those exceptions thrown from the try-with-resources statement are suppressed, and the exception thrown by the block is the one that is thrown by the writeToFileZipFileContents method. You can retrieve these suppressed exceptions by calling the Throwable.getSuppressed method from the exception thrown by the try block." ID="ID_1622931150" CREATED="1412166820272" MODIFIED="1412166820272"/>
</node>
</node>
<node TEXT="IOUtils.closeQuietly" ID="ID_1455537437" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="http://commons.apache.org/proper/commons-io/apidocs/org/apache/commons/io/IOUtils.html#closeQuietly%28java.io.Closeable...%29" ID="ID_1401379593" CREATED="1412166820272" MODIFIED="1412166820272" LINK="http://commons.apache.org/proper/commons-io/apidocs/org/apache/commons/io/IOUtils.html#closeQuietly%28java.io.Closeable...%29"/>
<node TEXT="Writer writer = null;&#xa;try {&#xa;    writer = writerSupplier.get();&#xa;    writer.append(&quot;test&quot;);&#xa;} finally {&#xa;    IOUtils.closeQuietly(writer);&#xa;}" ID="ID_458067029" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="Avantage:" ID="ID_1578835195" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="Simple" ID="ID_974435318" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="R&#xe9;pond au cas d&apos;utilisation le plus classique" ID="ID_640886753" CREATED="1412166820272" MODIFIED="1412166820272"/>
</node>
<node TEXT="Inconv&#xe9;nient:" ID="ID_1600217772" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="Close deux fois" ID="ID_262538041" CREATED="1412166820272" MODIFIED="1412166820272"/>
</node>
</node>
<node TEXT="Closer (Guava)" ID="ID_987542681" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="https://code.google.com/p/guava-libraries/wiki/ClosingResourcesExplained" ID="ID_908924625" CREATED="1412166820272" MODIFIED="1412166820272" LINK="https://code.google.com/p/guava-libraries/wiki/ClosingResourcesExplained"/>
<node TEXT="Closer closer = Closer.create();&#xa;try {&#xa;    FileWriter in = closer.register(writerSupplier.get());&#xa;    in.append(&quot;test&quot;);&#xa;} catch (Throwable e) {&#xa;    throw closer.rethrow(e);&#xa;} finally {&#xa;    closer.close();&#xa;}" ID="ID_298892253" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="Avantage:" ID="ID_493052739" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="Le comportement est plus proche de try with ressources" ID="ID_326492997" CREATED="1412166820272" MODIFIED="1412166820272"/>
</node>
<node TEXT="Inconvenient:" ID="ID_169417302" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="lourd" ID="ID_1720175542" CREATED="1412166820272" MODIFIED="1412166820272"/>
</node>
</node>
</node>
<node TEXT="Temps" ID="ID_797873875" CREATED="1412166820272" MODIFIED="1412166820272">
<icon BUILTIN="button_ok"/>
<node TEXT="http://docs.oracle.com/javase/8/docs/technotes/guides/datetime/index.html" ID="ID_1037252044" CREATED="1412166820272" MODIFIED="1412166820272" LINK="http://docs.oracle.com/javase/8/docs/technotes/guides/datetime/index.html">
<node TEXT="http://www.oracle.com/technetwork/articles/java/jf14-date-time-2125367.html" ID="ID_1363185991" CREATED="1412166820272" MODIFIED="1412166820272" LINK="http://www.oracle.com/technetwork/articles/java/jf14-date-time-2125367.html"/>
</node>
<node TEXT="www.joda.org/joda-time/" ID="ID_1512838546" CREATED="1412166820272" MODIFIED="1412166820272"/>
</node>
<node TEXT="Flux de donn&#xe9;es/Stream" ID="ID_1128521306" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="Guava" ID="ID_181943057" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="final List&lt;Integer&gt; l = Arrays.asList(1, 2, 3);&#xa;final List&lt;String&gt; transform = Lists.transform(l,&#xa;new Function&lt;Integer, String&gt;() {&#xa;  public String apply(final Integer input) {&#xa;    return input.toString();&#xa;  }&#xa;});&#xa;for (final String string : transform) {&#xa;  System.out.println(string);&#xa;}" ID="ID_1778191586" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="final HashMap&lt;Object, Object&gt; map = new HashMap&lt;&gt;();&#xa;map.put(&quot;key1&quot;, &quot;val1&quot;);&#xa;map.put(&quot;key2&quot;, &quot;val2&quot;);&#xa;final HashBiMap&lt;Object, Object&gt; biMap = HashBiMap.create(map);&#xa;System.out.println(biMap.inverse().get(&quot;val2&quot;).equals(&quot;key2&quot;));" ID="ID_1136333154" CREATED="1412166820272" MODIFIED="1412166820272"/>
</node>
</node>
<node TEXT="Strings in switch statements, l&apos;op&#xe9;rateur diamant" ID="ID_952634133" CREATED="1412166820272" MODIFIED="1412166820272">
<icon BUILTIN="help"/>
<icon BUILTIN="button_ok"/>
<icon BUILTIN="help"/>
<node TEXT="Des Ifs et des elses" ID="ID_869603446" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="Des r&#xe9;p&#xe9;titions" ID="ID_1723188046" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node ID="ID_192690200" CREATED="1412166820272" MODIFIED="1412166820272"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      D&#233;sol&#233;
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="jvisualVM (2m)" ID="ID_1860079844" CREATED="1412166820272" MODIFIED="1412166820272">
<icon BUILTIN="button_ok"/>
<node TEXT="Vous avez une licence jprofiler? alors n&apos;&#xe9;coutez pas" ID="ID_1674719212" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="JProfiler est mieux" ID="ID_218134578" CREATED="1412166820272" MODIFIED="1412166820272"/>
</node>
<node TEXT="Profiler: Permet de d&#xe9;tecter les endroits du code ou le programme passe du temps" ID="ID_242413664" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="D&#xe9;j&#xe0; pr&#xe9;sent dans le JDK (y compris le 6)" ID="ID_331509514" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="Se connecter &#xe0; la VM du programme qui prend du temps" ID="ID_794866517" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="Commencer un sampler ou un profiler" ID="ID_431894951" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="Lancer l&apos;execution" ID="ID_1244372663" CREATED="1412166820272" MODIFIED="1412166820272">
<node TEXT="De toutes fa&#xe7;ons, si &#xe7;a prend vraiment des heures, ce n&apos;est pas pendant le temps de connection + lancement du sampler" ID="ID_1296589409" CREATED="1412166820272" MODIFIED="1412166820272"/>
</node>
<node TEXT="Prendre un snapshot ou attendre la fin d&apos;execution" ID="ID_948600226" CREATED="1412166820272" MODIFIED="1412166820272"/>
<node TEXT="chercher l&apos;execution de sa m&#xe9;thode et constater" ID="ID_1455515071" CREATED="1412166820272" MODIFIED="1412166820272">
<hook URI="resources/visualvm.png" SIZE="1.0" NAME="ExternalObject"/>
</node>
</node>
</node>
</node>
</map>
